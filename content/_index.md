### Main Form

<div id="pipeline-form">
  <form action="#" method="post">
    <p>Choose the environment :</p>
    <select id="env">
      <option>QA</option>
      <option>DEV</option>
      <option>SupportDev</option>
      <option>Training</option>
      <option>EU Stage 1</option>
      <option>EU Stage 2</option>
      <option>CN Stage 1</option>
      <option>CN Stage 2</option>
    </select> 
    <br>
    <p>Choose the path :</p>
    <div>
   <input type="checkbox" id="contentpath" value="content">
   <label for="contentpath"> content </label><br>
   <input type="checkbox" id="designspath" value="designs">
   <label for="designspath"> designs </label><br>
   <input type="checkbox" id="assetspath" value="assets">
   <label for="assetspath"> assets </label><br>
   </div>
    <button type="button" id="api-example">Run the Pipeline</button>
  
</div>
<div id="pipeline-after" style="display:none;">
  <p id="api-output">Please stand by...</p>
</div>

</form>
